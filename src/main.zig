const clap = @import("clap");
const std = @import("std");
const zg = @import("ziglyph");
const term = @import("term.zig");
const kitty = @import("cat.zig");
const math = std.math;

const params = clap.parseParamsComptime(
    \\-h, --help                display this help and exit
    \\-l, --luminance   <real>  the LCH luminance value (default: 50)
    \\-c, --chroma      <real>  the LCH chroma value (default: 132)
    \\-f, --frequency   <int>   rainbow frequency (default: 4)
    \\-s, --start       <int>   rainbow start position 
    \\-a, --ambiguous   <amb>   how to handle ambiguous width characters, full or half (default: half)
    \\-C, --color               force color output when stdout is not a TTY
    \\<file>...                 files to lolcat, if input is not on stdin
    \\
);

const parsers = .{
    .file = clap.parsers.string,
    .real = clap.parsers.float(f32),
    .int = clap.parsers.int(u16, 0),
    .amb = clap.parsers.enumeration(zg.display_width.AmbiguousWidth),
};

pub fn main() !void {
    @setFloatMode(std.builtin.FloatMode.optimized);
    const std_out = std.io.getStdOut();
    const is_tty = std_out.isTty();
    var diag = clap.Diagnostic{};
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var res = clap.parse(clap.Help, &params, parsers, .{
        .diagnostic = &diag,
        .allocator = gpa.allocator(),
    }) catch |err| {
        try diag.report(std.io.getStdErr().writer(), err);
        std.process.exit(1);
    };
    defer res.deinit();

    if (res.args.help != 0) {
        try clap.help(std_out.writer(), clap.Help, &params, .{});
        std.process.exit(1);
    }

    var bw = std.io.bufferedWriter(std_out.writer());
    const w = bw.writer();
    const freq = if (res.args.frequency) |f| math.clamp(f, 1, kitty.num_color_entries - 1) else 4;

    var cat = kitty.Cat(@TypeOf(w)){
        .writer = w,
        .luminance = res.args.luminance orelse 50.0,
        .chroma = if (res.args.chroma) |c| math.clamp(c, 0.0, 150.0) else 132.0,
        .freq = freq,
        .use_color = is_tty or res.args.color != 0,
        .max_width = if (is_tty) @max(try term.getTermWidth(), 2) - 1 else math.maxInt(u15),
        .ambiguous = res.args.ambiguous orelse .half,
        .pos = .{
            .x = 0,
            .y = res.args.start orelse 0,
            .mod = @intCast(kitty.num_color_entries / freq),
        },
    };

    if (cat.use_color) {
        try cat.initColorTable();
    }

    const uffda = cat.lol(res.positionals);

    if (cat.use_color)
        try w.writeAll("\x1b[39m");

    try bw.flush();

    uffda catch |e| {
        const std_err = std.io.getStdErr();
        try std_err.writer().print("\r😿 there was a {s}\x1b[0K\n", .{@errorName(e)});
        std.process.exit(1);
    };
}

const testing = std.testing;

test {
    _ = @import("cat.zig");
}

test "term width on windows?" {
    const win = std.os.windows;

    const w = term.getTermWidth() catch {
        const eno = win.kernel32.GetLastError();
        _ = eno;
        //std.log.err("last error: {d}", .{eno});
        //   return err;
        return;
    };
    _ = w;

    //try testing.expect(0 < w);
}

test "leading zeros" {
    // this works with `atoi`, `strtol`, and also in zig,
    // and in every tested terminal emulator.
    const n = try std.fmt.parseInt(usize, "00012", 10);
    try std.testing.expectEqual(@as(usize, 12), n);
}
