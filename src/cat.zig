const std = @import("std");
const colorz = @import("colorz");
const zg = @import("ziglyph");
const build_options = @import("build-options");

pub const num_color_entries = 360;
const color_fmt = "\x1b[38;2;{d:0>3};{d:0>3};{d:0>3}m";
const entry_len: usize = @intCast(std.fmt.count(color_fmt, .{ 255, 255, 255 }));

pub fn Cat(comptime WriterT: type) type {
    return struct {
        writer: WriterT,
        color_data: [num_color_entries * entry_len]u8 = undefined,
        // options
        use_color: bool,
        freq: u32,
        luminance: f32,
        chroma: f32,
        max_width: u16,
        ambiguous: zg.display_width.AmbiguousWidth,
        // state
        pos: struct {
            x: usize,
            y: usize,
            mod: u16,
        },

        const Self = @This();

        pub fn initColorTable(self: *Self) !void {
            for (0..self.pos.mod) |i| {
                const col = self.colorForIndex(i);
                _ = try std.fmt.bufPrint(
                    self.color_data[i * entry_len ..],
                    color_fmt,
                    .{
                        colorz.fastIntFromFloat(u8, col.r),
                        colorz.fastIntFromFloat(u8, col.g),
                        colorz.fastIntFromFloat(u8, col.b),
                    },
                );
            }
        }

        pub fn lol(self: *Self, files: []const []const u8) !void {
            if (0 < files.len) {
                for (files) |fname| {
                    var f = try std.fs.cwd().openFile(fname, .{});
                    defer f.close();
                    try self.processFile(f);
                }
            } else {
                const stdin_file = std.io.getStdIn();
                try self.processFile(stdin_file);
            }
        }

        fn processFile(self: *Self, file: std.fs.File) !void {
            if (self.use_color) {
                var br = std.io.bufferedReader(file.reader());
                try self.writeGayBytesFromReader(br.reader());
            } else {
                var fifo = std.fifo.LinearFifo(u8, .{ .Static = 8192 }).init();
                try fifo.pump(file.reader(), self.writer);
            }
        }

        fn writeGayBytesFromReader(self: *Self, reader: anytype) !void {
            var state: u3 = 0;
            var utf8_buf: [4]u8 = undefined;
            var cp_buf: [2]?u21 = undefined;
            var display_width: ?isize = null;

            cp_buf[1] = try zg.readCodePoint(reader) orelse return;
            try self.writeNextColor();

            while (cp_buf[1]) |cur_cp| {
                cp_buf[0] = cur_cp;
                cp_buf[1] = try zg.readCodePoint(reader);

                if (display_width == null) {
                    var w = zg.display_width.codePointWidth(cur_cp, self.ambiguous);

                    if (display_width == null and w != 0) {
                        if (zg.emoji.isExtendedPictographic(cur_cp)) {
                            if (cp_buf[1] == 0xFE0E) w = 1;
                        }

                        display_width = w;
                    }
                }

                if (zg.graphemeBreak(cur_cp, cp_buf[1] orelse '\n', &state)) {
                    const new_x = self.pos.x + @max(0, display_width orelse 1);

                    if (self.max_width < new_x or isNewLine(cur_cp)) {
                        self.pos.x = 0;
                        self.pos.y += 1;
                    } else self.pos.x = new_x;

                    display_width = null;
                    try self.writeNextColor();
                }

                const n = try std.unicode.utf8Encode(cur_cp, &utf8_buf);
                try self.writer.writeAll(utf8_buf[0..n]);
            }
        }

        fn writeNextColor(self: *Self) !void {
            const ix = @mod(self.pos.x + self.pos.y, self.pos.mod);
            const col_esc = self.getColorSliceForIndex(ix);
            try self.writer.writeAll(col_esc);
        }

        fn colorForIndex(self: *Self, i: usize) colorz.rgb.Rgb(f32, void) {
            return colorz.lch.lch(
                self.luminance,
                self.chroma,
                @as(f32, @floatFromInt(i * self.freq)),
            ).toRgb().clamp();
        }

        fn getColorSliceForIndex(self: *Self, ix: usize) []const u8 {
            const start = ix * entry_len;
            std.debug.assert(start + entry_len < self.color_data.len);
            return self.color_data[start .. start + entry_len];
        }
    };
}

fn isNewLine(cp: u21) bool {
    return cp == '\n' or cp == '\r';
}

const testing = std.testing;

fn birthTestCat(writer: anytype) Cat(@TypeOf(writer)) {
    const cat = Cat(@TypeOf(writer)){
        .writer = writer,
        .pos = .{ .x = 0, .y = 0, .mod = 30 },
        .max_width = 8,
        .use_color = true,
        .freq = 4,
        .luminance = 50.0,
        .chroma = 132.0,
        .ambiguous = .half,
    };

    return cat;
}

var test_out_buf: [0x1_000]u8 = undefined;

test "grapheme wrap" {
    inline for (
        // each is 8 graphemes
        .{
            "12345678",
            // this guy is 2 wide
            "🧑‍🌾345678",
            "123🧑‍🌾678",
            "123456🧑‍🌾",
            "星取法決",
            // this longo is also 2
            "👨🏻‍👩🏻‍👦🏻‍👦🏻345678",
        },
    ) |in_buf| {
        inline for (.{
            // max widths and expected x/y in the end
            .{
                .w = 8,
                .x = 8,
                .y = 0,
            },
            .{
                .w = 7,
                .x = 0,
                .y = 1,
            },
        }) |pos| {
            var fbsi = std.io.fixedBufferStream(in_buf);
            var fbso = std.io.fixedBufferStream(&test_out_buf);
            const fbsow = fbso.writer();
            var cat = birthTestCat(fbsow);

            cat.max_width = pos.w;

            try cat.writeGayBytesFromReader(fbsi.reader());
            try testing.expectEqual(@as(usize, pos.x), cat.pos.x);
            try testing.expectEqual(@as(usize, pos.y), cat.pos.y);
        }
    }
}
