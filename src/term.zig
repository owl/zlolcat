const std = @import("std");

pub fn getTermWidth() !u16 {
    return switch (@import("builtin").target.os.tag) {
        .windows => getTermWidthWindows(),
        else => getTermWidthIoctl(),
    };
}

fn getTermWidthIoctl() !u16 {
    var termsz: std.posix.winsize = undefined;
    if (-1 == std.posix.system.ioctl(1, std.posix.system.T.IOCGWINSZ, @intFromPtr(&termsz)))
        return error.IoCtl;
    return termsz.col;
}

fn getTermWidthWindows() !u16 {
    const win = std.os.windows;
    const stdout_h = try win.GetStdHandle(win.STD_OUTPUT_HANDLE);
    var csbi: win.CONSOLE_SCREEN_BUFFER_INFO = undefined;

    // https://learn.microsoft.com/en-us/windows/console/getconsolescreenbufferinfo#return-value
    if (0 == win.kernel32.GetConsoleScreenBufferInfo(stdout_h, &csbi))
        return error.JustWindowsThings;

    const columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
    // it's signed? you can have negative columns?
    return @intCast(columns);
}
