const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "zlolcat",
        .root_source_file = b.path("src" ++ std.fs.path.sep_str ++ "main.zig"),
        .target = target,
        .optimize = optimize,
        .single_threaded = true,
        .strip = b.option(bool, "strip", "Omit debug symbols") orelse (optimize != .Debug),
    });

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);

    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");

    run_step.dependOn(&run_cmd.step);

    const unit_tests = b.addTest(.{
        .root_source_file = b.path("src" ++ std.fs.path.sep_str ++ "main.zig"),
        .target = target,
        .optimize = optimize,
    });

    if (target.result.os.tag == .windows)
        b.enable_wine = true;

    const run_unit_tests = b.addRunArtifact(unit_tests);
    const test_step = b.step("test", "Run unit tests");

    test_step.dependOn(&run_unit_tests.step);

    inline for (.{
        "clap",
        "colorz",
        "ziglyph",
    }) |m| {
        const d = b.dependency(m, .{
            .target = target,
            .optimize = optimize,
        });
        exe.root_module.addImport(m, d.module(m));
        unit_tests.root_module.addImport(m, d.module(m));
    }
}
