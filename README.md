# zlolcat 😸 [![builds.sr.ht status](https://builds.sr.ht/~alva/zlolcat/commits/trunk.svg)](https://builds.sr.ht/~alva/zlolcat?)
it's a speedy and pretty little lolcat.

originally based on [clolcat](https://github.com/IchMageBaume/clolcat), now it has diverged in some ways:
- multibyte characters and grapheme clusters are supported properly.
- LCH colorspace is used for a more pleasant and accessible output.
- parameters for luminance and chroma.
- parameter for handling of [ambiguous width characters](https://unicode.org/reports/tr11/#Ambiguous).
- yet over twice as fast in typical use, as measured by [hyperfine](https://github.com/sharkdp/hyperfine) on various inputs.

compared to [other great implementations](https://github.com/mazznoer/lolcrab)
that support unicode properly, zlolcat is 400%-1200% faster in typical use,
but slower on some edge-cases.

![screenshot showing multibyte support and LCH colors](share/screenshot.png)

## building
[install latest zig](https://github.com/ziglang/zig/wiki/Install-Zig-from-a-Package-Manager)
and do `zig build install --prefix ~/.local -Doptimize=ReleaseFast`.

## usage
install the program in your `$PATH`, and invoke `zlolcat` with something on `stdin`,
or `zlolcat file1 file2`.

use `zlolcat -h` to see all the options. to randomise the rainbow phase,
you can plug a random source in like `zlolcat -s$RANDOM`.

## acknowledgments
- unicode support is thanks to [ziglyph](https://codeberg.org/dude_the_builder/ziglyph).
- argument parsing is thanks to [clap](https://github.com/Hejsil/zig-clap).
