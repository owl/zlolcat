#!/bin/sh
set -eu

for f in "$@"
do
    printf 'doing %s\n' "$f"
    hyperfine -N --warmup 10 \
        'clolcat -fS1' \
        'zlolcat -C' \
        'lolcrab -S1' \
        --style color \
        --input "$f"
done

